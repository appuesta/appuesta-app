import React, {Component, Fragment} from 'react';
import {Text, View} from 'react-native';
import {Button, Input, Loading} from './common';
import axios from 'axios';
import deviceStorage from '../services/deviceStorage';

export default class Login extends Component {
  constructor(props: any) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: '',
      loading: false
    };

    this.loginUser = this.loginUser.bind(this);
    this.onLoginFail = this.onLoginFail.bind(this);
  }

  loginUser() {
    const {username, password} = this.state;

    this.setState({error: '', loading: true});

    axios.post("http://localhost:5000/api/login", {
      username: username,
      password: password
    })
      .then((response) => {
        const token = response.data.access_token;
        console.log("Token from the response: " + token);
        deviceStorage.saveKey("userToken", token);
        this.props.navigation.navigate('Matches', {userToken: token});
      })
      .catch((error) => {
        console.log(error);
        this.onLoginFail();
      });
  }

  onLoginFail() {
    this.setState({
      error: 'Login Failed',
      loading: false
    });
  }

  render() {
    const {username, password, error, loading} = this.state;
    const {form, section, errorTextStyle} = styles;

    return (
      <Fragment>
        <View style={form}>
          <View style={section}>
            <Input
              placeholder="user"
              label="User"
              value={username}
              onChangeText={username => this.setState({username})}
            />
          </View>

          <View style={section}>
            <Input
              secureTextEntry
              placeholder="password"
              label="Password"
              value={password}
              onChangeText={password => this.setState({password})}
            />
          </View>

          <Text style={errorTextStyle}>
            {error}
          </Text>

          {!loading ?
            <Button onPress={this.loginUser}>
              Login
            </Button>
            :
            <Loading size={'large'}/>
          }

        </View>
      </Fragment>
    );
  }
}

const styles = {
  form: {
    width: '100%',
    borderTopWidth: 1,
    borderColor: '#ddd',
  },
  section: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#ddd',
  },
  errorTextStyle: {
    alignSelf: 'center',
    fontSize: 18,
    color: 'red'
  }
};
