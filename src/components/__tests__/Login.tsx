import Login from '../Login';
import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {Button, Input} from '../common';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter'
import sinon from 'sinon'
import deviceStorage from "../../services/deviceStorage";

const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn()
  },
  ...props
});

describe("Login", () => {
  describe("rendering", () => {
    let wrapper: ShallowWrapper;
    let props: any;   // use type "any" to opt-out of type-checking
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<Login {...props} />);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('should render 2 input fields and 1 button/>', () => {
      expect(wrapper.find(Input)).toHaveLength(2);
      expect(wrapper.find(Button)).toHaveLength(1);
    });
  });

  describe("Sign in", () => {
    let wrapper: ShallowWrapper;
    let props: any;   // use type "any" to opt-out of type-checking
    let mockAxio = new MockAdapter(axios);

    it('should render an error message when login with invalid credentials', async () => {
      mockAxio.onPost('http://localhost:5000/api/login').reply(401);
      props = createTestProps({});
      wrapper = shallow(<Login {...props} />);

      const spy = sinon.spy();
      spy(Login.prototype, 'onLoginFail');

      wrapper.setState({username: 'invalid'});
      wrapper.setState({password: 'invalid'});

      await wrapper.find(Button).first().props().onPress();
      expect(spy.calledOnce).toBeTruthy();
    });

    it('should redirect to Matches with user token', async () => {
      mockAxio.onPost('http://localhost:5000/api/login').reply(200, {
        access_token: 'token'
      });
      props = createTestProps({});
      deviceStorage.saveKey = jest.fn();
      wrapper = shallow(<Login {...props} />);
      const spy = sinon.spy();
      spy(Login.prototype, 'this.props.navigation.navigate');

      wrapper.setState({username: 'valid'});
      wrapper.setState({password: 'valid'});

      await wrapper.find(Button).first().props().onPress();
      expect(spy.calledOnce).toBeTruthy();
    });
  });
});
