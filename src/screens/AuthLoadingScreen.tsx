import React from 'react';
import {ActivityIndicator, AsyncStorage, StatusBar, View,} from 'react-native';

export default class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  getToken = async () => {
    var value, collect = null;
    try{
      value = await AsyncStorage.getItem('userToken').then(
        (values) => {
          collect = values;
        });
    } catch (error) {
      console.log('Error: ',error);
    }
    return collect;
  };

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = () => {
    this.getToken().then((token)=> {
      this.props.navigation.navigate(token ? 'Matches' : 'Auth', {userToken: token});
    })
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator/>
        <StatusBar barStyle="default"/>
      </View>
    );
  }
}
