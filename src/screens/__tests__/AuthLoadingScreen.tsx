import {shallow, ShallowWrapper} from "enzyme";
import AuthLoadingScreen from "../AuthLoadingScreen";
import React from "react";
var sandbox = require('sinon').createSandbox();
import {AsyncStorage} from "react-native";


const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn()
  },
  ...props
});

describe("AuthLoading", () => {
  let wrapper: ShallowWrapper;
  let props: any;   // use type "any" to opt-out of type-checking
  beforeEach(() => {
    props = createTestProps({});
    wrapper = shallow(<AuthLoadingScreen {...props} />);
  });

  afterEach(function () {
    // Restore all the things made through the sandbox
    sandbox.restore();
  });

  it('not token you are redirected to Login Page', async () => {
    sandbox.stub(AsyncStorage, 'getItem').rejects();
    const spy = sandbox.spy();
    spy(AuthLoadingScreen.prototype, 'this.props.navigation.navigate');

    expect(await wrapper.instance().getToken()).toBeNull();
    expect(spy.calledOnce).toBeTruthy();
  });

  it('there is a token you are redirected to Matches Page', async () => {
    sandbox.stub(AsyncStorage, 'getItem').resolves('token');
    const spy = sandbox.spy();
    spy(AuthLoadingScreen.prototype, 'this.props.navigation.navigate');

    expect(await wrapper.instance().getToken()).toEqual('token');
    expect(spy.calledOnce).toBeTruthy();
  });
});
