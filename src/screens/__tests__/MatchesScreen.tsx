import {shallow, ShallowWrapper} from "enzyme";
import MatchesScreen from "../MatchesScreen";
import React from "react";
import sinon from "sinon";
import {Server} from 'mock-socket';

const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn(),
    getParam: jest.fn(() => 'token'),
  },
  ...props
});

describe("Matches", () => {
  let wrapper: ShallowWrapper;
  let props: any;   // use type "any" to opt-out of type-checking
  let fakeURL = 'ws://localhost:5000/socket';
  let mockServer = new Server(fakeURL);

  beforeEach(() => {

    mockServer.on('connect', () => {
      console.log('Connected');
    });
    mockServer.on('get_matches', data => {
      mockServer.emit('new_matches', {home_team: 'A', away_team: 'B', home_score: 0, away_score: 0, id: 1});
    });
    props = createTestProps({});
    wrapper = shallow(<MatchesScreen {...props} />);
  });

  afterEach(() => {
    mockServer.stop();
    wrapper.unmount();
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('Has to be empty when it\'s rendering', () => {
    const spy = sinon.spy();
    spy(MatchesScreen.prototype, '_showEmptyListView');
    expect(spy.calledOnce).toBeTruthy();
  });

  it('Connects to the socket when component is mounted', async () => {
    const spy = sinon.spy();
    spy(MatchesScreen.prototype, 'onReceivedMessage');
    mockServer.emit('new_matches', {home_team: 'A', away_team: 'B', home_score: 0, away_score: 0, id: 1});
    expect(spy.calledOnce).toBeTruthy();
  })
});
