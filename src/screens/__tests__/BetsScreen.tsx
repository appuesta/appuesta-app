import {shallow, ShallowWrapper} from "enzyme";
import BetScreen from "../BetScreen";
import React from "react";
import {Text, Button, Alert} from 'react-native';
import sinon from 'sinon';
import Login from "../../components/Login";
import MockAdapter from 'axios-mock-adapter'
import axios from "axios";

const getParams = sinon.stub();
getParams.withArgs('userToken').returns('token');
getParams.withArgs('title').returns('A - B');
getParams.withArgs('match').returns({home_team: 'A', away_team: 'B', home_score: 0, away_score: 0, id: 1})

const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn(),
    getParam: getParams
  },
  ...props
});

describe("Bets", () => {
  let wrapper: ShallowWrapper;
  let props: any;   // use type "any" to opt-out of type-checking

  beforeEach(() => {
    props = createTestProps({});
    wrapper = shallow(<BetScreen {...props} />);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('Has to show match and result', () => {
    expect(wrapper.find(Text).first().render().text()).toEqual('A - B');
    expect(wrapper.find(Text).at(1).render().text()).toEqual('0 - 0');
  });

  it('Send an empty result shows an alert', () => {
    const spy = sinon.spy();
    spy(Login.prototype, 'Alert.alert');
    wrapper.find(Button).first().props().onPress();
    expect(spy.calledOnce).toBeTruthy();
  });

  it('Send a valid result sends a request to the server', async () =>{
    const spy = sinon.spy();
    spy(Login.prototype, 'sendResult');
    let mockAxio = new MockAdapter(axios);
    mockAxio.onPost('http://127.0.0.1:5000/api/bets').reply(200, {
      result: 'ok'
    });
    wrapper.setState({result: 1});
    await wrapper.find(Button).first().props().onPress();
    expect(spy.calledOnce).toBeTruthy();
  })
});
