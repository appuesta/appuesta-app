import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import io from 'socket.io-client/dist/socket.io.js';


export default class MatchesScreen extends Component {
  static navigationOptions = {
    title: 'Matches'
  };

  socket: any;

  constructor(props: any) {
    super(props);
    this.state = {
      matches: [],
      userToken: this.props.navigation.getParam("userToken", "token")
    };
  }

  componentDidMount() {
    this.socketConnect();
  }

  componentWillUnmount() {
    this.socket.off('new_matches', this.onReceivedMessage);
    this.socket.off('connect', this.socketConnect);
    this.socket.close();
  }

  storeMessages = (matches: any) => {
    if (matches.length > 0) {
      this.setState(
        () => {
          const matchesArr: any[] = [];

          matches.map((item: any) => {
            const matchModel = {
              home_team: item.home_team,
              away_team: item.away_team,
              home_score: item.home_score,
              away_score: item.away_score,
              id: item.id
            };
            matchesArr.push(matchModel);
          });

          return {
            matches: matchesArr,
          };
        }
      );
    }
  };

  onReceivedMessage = (matches: any) => {
    console.log(matches);
    this.storeMessages(matches);
  };

  socketConnect() {
    this.socket = io('http://localhost:5000/socket', {transports: ['websocket']});
    this.socket.on('connect', () => {
      console.log('connect');
      this.socket.emit('get_matches', {msg: 'app'});
    });
    this.socket.on('new_matches', this.onReceivedMessage);
  }

  _keyExtractor = (item, index) => index;


  _renderItem = ({item}) => {
    return (
      <TouchableHighlight
        onPress={() => {
          this.props.navigation.navigate('Bets', {
            userToken: this.state.userToken,
            match: item,
            title: `${item.home_team} - ${item.away_team}`
          });
        }}
        underlayColor='#dddddd'>
        <View>
          <View style={styles.rowContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.title}
                    numberOfLines={1}>{item.home_team} {item.home_score} - {item.away_score} {item.away_team}</Text>
            </View>
          </View>
          <View style={styles.separator}/>
        </View>
      </TouchableHighlight>
    );
  };

  _showEmptyListView() {
    return (
      <View>
        <View style={styles.rowContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.title}
                  numberOfLines={1}>Loading ...</Text>
          </View>
        </View>
        <View style={styles.separator}/>
      </View>
    );
  }

  render() {
    const {matches} = this.state;
    return (
      <FlatList
        data={matches}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        ListEmptyComponent={this._showEmptyListView()}
      />
    );
  }
}

const styles = StyleSheet.create({
  thumb: {
    width: 80,
    height: 80,
    marginRight: 10
  },
  textContainer: {
    flex: 1
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  title: {
    fontSize: 20,
    color: '#656565'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10
  },
});
