import React, {Component} from 'react';
import {Alert, AsyncStorage, Button, Text, View} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

export default class BetScreen extends Component {

  constructor(props: any) {
    super(props);
    this.state = {
      result: '',
      userToken: this.props.navigation.getParam("userToken", "token")
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('title', 'Live Match'),
    };
  };

  _removeItemValue = async () => {
    try {
      await AsyncStorage.removeItem('userToken');
    } catch (exception) {
      console.log(error);
    }
    this.props.navigation.navigate('Auth')
  };

  sendResult(MatchId) {
    const {userToken, result} = this.state;
    const data = {
      match: MatchId.toString(),
      winner: result,
    };
    const options = {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': `Bearer ${userToken}`
      }
    };
    console.log('Token for send result: ' + userToken);
    const axios = require('axios');
    axios.post('http://127.0.0.1:5000/api/bets', data, options).then(function (response) {
      console.log('Response: ' + response.data.result);
      Alert.alert('Your response was saved!')
    })
      .catch(function (error) {
        console.error(error);
        Alert.alert('Something was wrong :(')
      });
  }

  render() {
    const {navigation} = this.props;
    const match = navigation.getParam('match');
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>{match.home_team} - {match.away_team}</Text>
        <Text>{match.home_score} - {match.away_score}</Text>
        <View>
          <RNPickerSelect
            placeholder={{
              label: 'Select a result for the match...',
              value: null,
              color: 'red',
            }}

            onValueChange={(value) =>
              this.setState({result: value})
            }
            items={[
              {label: `${match.home_team}`, value: 'W'},
              {label: 'Draw', value: 'D'},
              {label: `${match.away_team}`, value: 'L'},
            ]}
          />
        </View>
        <Button
          title="Send Result"
          onPress={() => {
            if (this.state.result) {
              this.sendResult(match.id);
            } else {
              Alert.alert('Please select a result :)')
            }
          }}
        />
      </View>
    );
  }
}
