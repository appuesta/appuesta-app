[![pipeline status](https://gitlab.com/je.sanchezb/appuesta/badges/master/pipeline.svg)](https://gitlab.com/je.sanchezb/appuesta/commits/master)
[![coverage report](https://gitlab.com/je.sanchezb/appuesta/badges/master/coverage.svg)](https://gitlab.com/je.sanchezb/appuesta/commits/master)

# APPuesta

A react-native app for learning

## How to use it

You have to use [yarn](https://yarnpkg.com/lang/en/) to install dependencies and [Expo](https://expo.io/) for generating the app and test it using a simulator or a real device.

```bash
$ yarn install
$ expo start
```
To test the app you need to have a simulator/emulator/real device connected to the Expo's development server.
- If you want to deploy in a real Android or iPhone device follow the next [instructions.](https://docs.expo.io/versions/v35.0.0/get-started/create-a-new-app/#opening-the-app-on-your-phonetablet).
- For Android emulators you can use [this](https://medium.com/@rishii.kumar.chawda/install-android-emulator-for-react-native-app-without-installing-android-studio-727d7734528)
- For iOS simulators you need to install XCode and install the simulator. You can follow this [instructions](https://hackernoon.com/manually-install-ios-simulators-in-xcode-f7e4bbe50753)

## Testing

You can use two different tools:
- `eslint`: linter
- `jest`: for testing

```bash
$ yarn eslint App.tsx src/ --fix    # To execute the linter in all your files
$ yarn testFinal                    # To execute all the tests 
```
