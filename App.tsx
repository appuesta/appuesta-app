import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MatchesScreen from './src/screens/MatchesScreen';
import BetsScreen from './src/screens/BetScreen';
import AuthScreen from './src/screens/AuthScreen';
import AuthLoadingScreen from './src/screens/AuthLoadingScreen';

const AppStack = createStackNavigator({ Matches: MatchesScreen, Bets: BetsScreen });
const AuthStack = createStackNavigator({ Login: AuthScreen });

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);
